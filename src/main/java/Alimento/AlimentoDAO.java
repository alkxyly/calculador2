package Alimento;

import java.util.List;

public interface AlimentoDAO {
	public void salvar(Alimento alimento);
	public void excluir(Alimento alimento);
	public void atualizar(Alimento alimento);
	public Alimento carregar(Integer id);
	public List<Alimento> listar();
	public List<Alimento> listarPorCodigo(String codigo);

}

package AssociarAlimento;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import Alimento.Alimento;
import AlimentoNutrientes.AlimentoNutriente;
import AlimentoNutrientes.AlimentoNutrienteRN;
import FiltrosBusca.NutrienteFilter;
import Nutriente.Nutriente;
import Nutriente.NutrienteRN;

@ManagedBean(name = "associarBean")
@ViewScoped
public class AssociarBean {
	private final String PESQUISA_ALIMENTO_JSF = "/admin/alimentos/PesquisarAlimentos";

	private Alimento alimento;
	private List<Nutriente> listaNutriente;
	private List<Nutriente> todosNutrientes;
	private int qtd_nutrientes;
	
	private NutrienteFilter nutrienteFilter =  new NutrienteFilter();

	@PostConstruct
	public void init() {
		this.alimento = (Alimento) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("alimento");

		NutrienteRN nutrienteRN = new NutrienteRN();
		List<Nutriente> naoAssociados = new ArrayList<Nutriente>();
		naoAssociados =	nutrienteRN.listarNaoAssociados(this.alimento);		
		this.todosNutrientes = nutrienteRN.listar(nutrienteFilter);
		
		if(this.alimento.getAlimentoNutriente().size() > 0){//verifica se o alimento esta assiciado
			this.listaNutriente = new ArrayList<Nutriente>();
			for (int i = 0; i < this.alimento.getAlimentoNutriente().size(); i++) {
				Nutriente n = this.alimento.getAlimentoNutriente().get(i).getNutriente();
				double valorNutritivo = this.alimento.getAlimentoNutriente().get(i).getValorNutritivo();
				int uni = this.alimento.getAlimentoNutriente().get(i).getUnidade();
				n.setUnidade(uni);
				n.setValor_nutritivo(valorNutritivo);
				this.listaNutriente.add(n);
			}
			for (int i = 0; i < naoAssociados.size(); i++) {
				Nutriente naoAssociado = naoAssociados.get(i);			
				naoAssociado.setUnidade(0);
				naoAssociado.setValor_nutritivo(0.0);
				this.listaNutriente.add(naoAssociado);
			}
		
		}else
			this.listaNutriente = nutrienteRN.listar(nutrienteFilter);		

		this.qtd_nutrientes = 0;
	}
	/**
	 * @author alkxly
	 * 
	 * Realiza chamada a regra de negocio 
	 * referente a fazer a lógica de associação
	 * @return página de pesquisa de alimento
	 */
	public String associar(){
		//conterá todos os nutrientes que tiveram o valor nutritivo alterado.
		AlimentoNutrienteRN alimentoNutrienteRN =  new AlimentoNutrienteRN();
		List<AlimentoNutriente> alimentoNutrientes =  new ArrayList<AlimentoNutriente>();
		for (int i = 0; i < listaNutriente.size(); i++) { //criar uma RN 
			AlimentoNutriente alimentoNutriente = null;		
			if(listaNutriente.get(i).getValor_nutritivo() != 0 ){// campo alterado				
				alimentoNutriente = new AlimentoNutriente();
				Nutriente nutriente  = this.listaNutriente.get(i);
				double valorNutritivo = this.listaNutriente.get(i).getValor_nutritivo();
				int unidade = this.listaNutriente.get(i).getUnidade();

				alimentoNutriente.setAlimento(this.alimento);
				alimentoNutriente.setNutriente(nutriente);
				alimentoNutriente.setValorNutritivo(valorNutritivo);
				alimentoNutriente.setUnidade(unidade);

				alimentoNutrientes.add(alimentoNutriente);
			}
		}
		//criar uma RN para isso
		for (int i = 0; i <alimentoNutrientes.size(); i++) {
			alimentoNutrienteRN.salvar(alimentoNutrientes.get(i));
		}

		return PESQUISA_ALIMENTO_JSF;
	}

	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}
	public List<Nutriente> getListaNutriente() {
		return listaNutriente;
	}
	public void setListaNutriente(List<Nutriente> listaNutriente) {
		this.listaNutriente = listaNutriente;
	}

	public int getQtd_nutrientes() {
		this.qtd_nutrientes = this.listaNutriente.size();
		return qtd_nutrientes;
	}
	public void setQtd_nutrientes(int qtd_nutrientes) {
		this.qtd_nutrientes = qtd_nutrientes;
	}
	public NutrienteFilter getNutrienteFilter() {
		return nutrienteFilter;
	}
	public void setNutrienteFilter(NutrienteFilter nutrienteFilter) {
		this.nutrienteFilter = nutrienteFilter;
	}




}

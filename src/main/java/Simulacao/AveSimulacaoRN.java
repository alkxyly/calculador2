package Simulacao;

import java.util.List;

import AlimentoNutrientes.AlimentoNutriente;
import Origem.Origem;

/**
 * 
 * @author alkxyly
 *
 *         Nomenclatura e fórmulas
 * 
 *         ENN (Extrato não nitrogenado) = ((Matéria seca)-(Proteína
 *         bruta)-(Gordura)-(Matéria mineral)-(Fibra bruta)) ENNd (Extrato não
 *         nitrogenado digestível) = ((Coef. ENN)(ENN))/100 ENDF (Extrato não
 *         nitrogenado digestível + Fibra bruta) =((ENN)-(Coef. ENN)+(Fibra
 *         bruta))
 */
public class AveSimulacaoRN {

	/**
	 * Calcula a energia metabolizada para aves jovens
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia metabolizada
	 */
	public double calcularAvesJovensEnergiaMetabolizada(List<AlimentoNutriente> listaNutrientes, Origem orig) {
		Origem origem = orig;
		double eMaves = 0.0;

		if (origem.getNome().equals("Vegetal")) {
			eMaves = avesJovensEnergiaMetabolizadaVegetal(listaNutrientes);
		} else {
			eMaves = Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(listaNutrientes);
		}

		return eMaves;
	}

	/**
	 * Calcula a energia metabolizada para aves jovens
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia metabolizada
	 */
	public double calcularGalinhasEnergiaMetabolizada(List<AlimentoNutriente> listaNutrientes, Origem orig) {
		Origem origem = orig;
		double eMaves = 0.0;

		if (origem.getNome().equals("Vegetal")) {
			eMaves = GalinhasEnergiaMetabolizadaVegetal(listaNutrientes);
		} else {
			//eMaves = Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(listaNutrientes);
		}

		return eMaves;
	}

	/**
	 * 
	 * @author Rodrigo
	 * 
	 * @param avesJovensEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para aves jovens com alimentos de
	 *         origem vegetal Metodo para calcular energia para aves jovens com
	 *         alimento de origem vegetal
	 * 
	 */
	
	public Double avesJovensEnergiaMetabolizadaVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double ENN = 0.0; // Extrato n�o nitrog�nado
		Double ENNd = 0.0; // Coeficiente de digestibilidade de Extrato n�o nitrog�nado
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("144")) {
				ENN = alimentoNutriente.getValorNutritivo();
				System.out.println("Extrato n�o nitrog�nado " + ENN);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("730")) {
				ENNd = alimentoNutriente.getValorNutritivo();
				System.out.println("Extrato n�o nitrog�nado disgetivel " + ENNd);
			}

		}

		EMaves = (4.31 * (PB *(PBd/100))) + (9.29 * (g*(GD/100))) + (4.14 *(ENN* (ENNd/100)));
		System.out.println("Energia metabolizavel aves jovens " + EMaves);

		return EMaves*10;
	}

	/**
	 * 
	 * @author Rodrigo
	 * @param GalinhasEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para galinhas com alimentos de
	 *         origem vegetal
	 * 
	 *         Metodo para calcular energia para galinhas com alimento de origem
	 *         vegetal
	 */
	public Double GalinhasEnergiaMetabolizadaVegetal(List<AlimentoNutriente> listaNutrientes) {
		Double PB = 0.0; // proteina bruta
		Double Fibra = 0.0; // Fibra bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double ENN = 0.0; // Extrato n�o nitrog�nado
		Double ENNd = 0.0; // Coeficiente de digestibilidade de Extrato n�o nitrog�nado
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta " + PB);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura bruta " + g);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				Fibra = alimentoNutriente.getValorNutritivo();
				System.out.println("Fibra bruta " + Fibra);
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("144")) {
				ENN = alimentoNutriente.getValorNutritivo();
				System.out.println("Extrato n�o nitrog�nado " + ENN);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("730")) {
				ENNd = alimentoNutriente.getValorNutritivo();
				System.out.println("Extrato n�o nitrog�nado disgetivel " + ENNd);
			}

		}

		EMaves = (4.31 * (PB *(PBd/100))) + (9.29 * (g*(GD/100))) + (4.14 *(ENN* (ENNd/100))) + (0.3*((ENN*((100-ENNd)/100))+Fibra));
		
		//
		// ENDF(ENN n�o digerido) - (0.3*((ENN*((100-ENNd)/100))+Fibra))
		//
		
		System.out.println("A "+(4.31 * (PB *(PBd/100))) + (9.29 * (g*(GD/100))) + (4.14 *(ENN* (ENNd/100))) );
		System.out.println("B"+(0.3*(4.14 *(ENN* (ENNd/100)))+Fibra));
		System.out.println("Energia metabolizavel aves jovens " + EMaves);

		return EMaves*10;
	}

	/**
	 * 
	 * @author Rodrigo
	 * 
	 * @param GalinhasEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para aves jovens e galinhas com
	 *         alimentos de origens animal , vegetal entre outros.
	 * 
	 *         Metodo para calcular energia para galinhas e aves jovens com
	 *         alimentos de origem animal, vegetais entre outros.
	 */

	public Double Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		// Double ENN = 0.0; // Extrato n�o nitrog�nado
		// Double ENNd = 0.0; // Extrato n�o nitrog�nado disgetivel
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
				System.out.println("Proteina bruta digestivel " + PBd);
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
				System.out.println("Gordura digestivel " + GD);
			}

		}

		EMaves = (4.31 * PBd) + (9.29 * GD);
		System.out.println("Energia metabolizavel Galinhas e Aves jovens " + EMaves);

		return EMaves;
	}
}

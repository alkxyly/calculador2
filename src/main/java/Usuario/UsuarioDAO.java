package Usuario;

import java.util.List;



public interface UsuarioDAO {
	public void salvar(Usuario alimentoNutriente);
	public void excluir(Usuario alimentoNutriente);
	public void atualizar(Usuario alimentoNutriente);
	public Usuario carregar(Integer id);
	public List<Usuario> listar();
}

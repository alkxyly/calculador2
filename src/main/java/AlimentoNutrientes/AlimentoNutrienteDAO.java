package AlimentoNutrientes;

import java.util.List;

import Alimento.Alimento;


public interface AlimentoNutrienteDAO {
	public void salvar(AlimentoNutriente alimentoNutriente);
	public void excluir(AlimentoNutriente alimentoNutriente);
	public void atualizar(AlimentoNutriente alimentoNutriente);
	public AlimentoNutriente carregar(Integer id);
	public List<AlimentoNutriente> listar();
	public boolean isAlimentoAssociado(Alimento alimento);
	public List<AlimentoNutriente> listaComAlimento(Alimento alimento);
}

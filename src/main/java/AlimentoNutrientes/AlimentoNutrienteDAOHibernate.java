package AlimentoNutrientes;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Alimento.Alimento;

public class AlimentoNutrienteDAOHibernate implements AlimentoNutrienteDAO {
	private Session session ;


	public void setSession(Session session) {
		this.session = session;

	}

	@Override
	public void salvar(AlimentoNutriente alimentoNutriente) {
		this.session.save(alimentoNutriente);
	}

	@Override
	public void excluir(AlimentoNutriente alimentoNutriente) {
		

	}

	@Override
	public void atualizar(AlimentoNutriente alimentoNutriente) {
		// TODO Auto-generated method stub

	}

	@Override
	public AlimentoNutriente carregar(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AlimentoNutriente> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAlimentoAssociado(Alimento alimento) {
		boolean isAssociado = false;		
		Integer id = alimento.getId();
		
		Query query = this.session.createQuery("from AlimentoNutriente al where al.alimento.id = :id "); 
		query.setParameter("id", id );
		
		List<AlimentoNutriente> alimentoNutriente = query.list();
	
		if(alimentoNutriente.size() > 0){
			isAssociado = true;
		}else 
			isAssociado = false;
		
		return isAssociado;
	}

	
	@Override
	public List<AlimentoNutriente> listaComAlimento(Alimento alimento) {
		Query query = this.session.createQuery("from AlimentoNutriente al where al.alimento.id = :id and al.nutriente.codigo IN(111,123,144,121,700,710,730,100,151,141)");
		query.setParameter("id", alimento.getId());
		
		List<AlimentoNutriente> alimentoNutriente = query.list();
		return alimentoNutriente;
	}

}

package Nutriente;

import java.util.List;

import Alimento.Alimento;
import FiltrosBusca.NutrienteFilter;
import Util.DAOFactory;

public class NutrienteRN {
	private NutrienteDAO nutrienteDAO ;
	
	public NutrienteRN(){
		this.nutrienteDAO = DAOFactory.criarNutrienteDAO();
	}
	/**
	 * @author alkxly
	 * @since 15 de setembro de 2015
	 * @version 1.0
	 * @param nutriente - Objeto nutriente que será persistido no banco de dados.
	 */
	public void salvar(Nutriente nutriente){
		this.nutrienteDAO.salvar(nutriente);
	}
	public void atualizar(Nutriente nutriente){
		this.nutrienteDAO.atualizar(nutriente);
	}
	public void excluir(Nutriente nutriente){
		this.nutrienteDAO.excluir(nutriente);
	}
	/**
	 * @author alkxly
	 * @since 16 de setembro de 2015
	 * @version 1.0
	 * @return lista com todos os nutriente cadastrados no banco de dados.
	 */
	public List<Nutriente> listar(NutrienteFilter nutrienteFilter){
		String codigo = nutrienteFilter.getCodigo();
		
		if(!(codigo.equals(""))){
			return this.nutrienteDAO.listarPorCodigo(codigo);
		}else
			return this.nutrienteDAO.listar();
	}
	public List<Nutriente> listarNaoAssociados(Alimento alimento){
		return this.nutrienteDAO.listarNaoAssociados(alimento);
	}

}

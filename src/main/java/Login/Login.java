package Login;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

@Entity
@Table(name = "")
public class Login implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3794198595310120638L;
	
	@Id
	@GeneratedValue
	@Column(name = "")
	private int id;
	@Column(name = "")
    private String nome;
	@Column(name = "")
    private String email;
	@Column(name = "")
    private String senha;
	@Column(name = "")
    private boolean administrador;

    public Login() {
        this(0, null, null, null, false);
    }

    public Login(int id, String nome, String email, String senha, boolean administrador) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.administrador = administrador;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Login other = (Login) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}

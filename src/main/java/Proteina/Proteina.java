package Proteina;

import java.io.Serializable;

import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

@Entity
@Table( name = "" )
public class Proteina implements Serializable{
    
	private static final long serialVersionUID = -9214535420228143725L;
	
	@Id
	@GeneratedValue
	@Column(name = "")
    private int id;
	@Column(name = "")
    private int ida;// ID alimento
	@Column(name = "")
    private String codigo;
	@Column(name = "")
    private String nome;
	@Column(name = "")
    private double proteina_padrao; // Proteina padrão
    
    public Proteina(){}

    public Proteina(int id_alimento, String codigo, String nome, double proteina_padrao) {
        this.ida = id_alimento;
        this.codigo = codigo;
        this.nome = nome;
        this.proteina_padrao = proteina_padrao;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIda() {
        return ida;
    }

    public void setIda(int ida) {
        this.ida = ida;
    }

  
  

    public double getProteina_padrao() {
        return proteina_padrao;
    }

    public void setProteina_padrao(double proteina_padrao) {
        this.proteina_padrao = proteina_padrao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
   
    
}

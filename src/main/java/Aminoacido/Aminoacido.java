package Aminoacido;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import AlimentoNutrientes.AlimentoNutriente;

/**
 *
 * @author Rodrigo
 */

@Entity
@Table(name = "tb_aminoacidos")
public class Aminoacido implements Serializable {

	private static final long serialVersionUID = -7528287434068107194L;

	@Id
	@GeneratedValue
	@Column(name = "id_Aminoacidos")
	private Integer id;
	
	private String codigo;
	private String nome;
	
	private double valor;
	private String categoria;
	
	
	@ManyToOne
	@JoinColumn(name = "id")
	private AlimentoNutriente alimentoNutriente;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public AlimentoNutriente getAlimentoNutriente() {
		return alimentoNutriente;
	}

	public void setAlimentoNutriente(AlimentoNutriente alimentoNutriente) {
		this.alimentoNutriente = alimentoNutriente;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((alimentoNutriente == null) ? 0 : alimentoNutriente
						.hashCode());
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aminoacido other = (Aminoacido) obj;
		if (alimentoNutriente == null) {
			if (other.alimentoNutriente != null)
				return false;
		} else if (!alimentoNutriente.equals(other.alimentoNutriente))
			return false;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (Double.doubleToLongBits(valor) != Double
				.doubleToLongBits(other.valor))
			return false;
		return true;
	}	

}

package eventos;

import javax.faces.bean.ManagedBean;
 
@ManagedBean(name = "basic")
public class BasicView {
     
    private String text = "a";
 
    public String getText() {
        return text;
    }
    public void setText(String text) {
        junta(text);
    }
    
    public void junta(String text){
    	if(this.text.equals("a")){
    		this.text = text;
    	}else{
    	this.text = this.text+" "+text;
    	}
    }
}
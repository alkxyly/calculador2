package Endereco;

import java.util.List;

public interface EnderecoDAO {
	public void salvar(Endereco alimentoNutriente);
	public void excluir(Endereco alimentoNutriente);
	public void atualizar(Endereco alimentoNutriente);
	public Endereco carregar(Integer id);
	public List<Endereco> listar();
}

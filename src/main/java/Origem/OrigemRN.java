package Origem;

import java.util.List;
import Util.DAOFactory;

public class OrigemRN {
	private OrigemDAO origemDAO ;
	
	public OrigemRN(){
		this.origemDAO = DAOFactory.criarOrigemDAO();
	}
	/**
	 * @author Rodrigo
	 */
	public void salvar(Origem origem){
		this.origemDAO.salvar(origem);
	}
	public void atualizar(Origem origem){
		this.origemDAO.atualizar(origem);
	}
	public void excluir(Origem origem){
		this.origemDAO.excluir(origem);
	}
	public List<Origem> listar(){
		return origemDAO.listar();
	}

}
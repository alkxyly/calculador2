package Origem;


import java.util.List;

import org.hibernate.Session;



public class OrigemDAOHibernate  implements OrigemDAO{
	private Session session;
	
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Origem origem) {
		this.session.save(origem);
	}

	@Override
	public void excluir(Origem origem) {
		this.session.delete(origem);
	}

	@Override
	public void atualizar(Origem origem) {
		this.session.update(origem);		
	}

	@Override
	public Origem carregar(Integer id) {
		return null;
	}

	@Override
	public List<Origem> listar() {
		return this.session.createCriteria(Origem.class).list();
	}
}
